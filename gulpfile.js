const gulp = require('gulp');
const uglify = require('gulp-terser');
const del = require('del');

gulp.task('clean', () => {
  return del('./build');
});

gulp.task('build-js', done => {
  gulp.src(['./src/**/*.js'])
    .pipe(uglify())
    .pipe(gulp.dest('./build'));
  return done();
});

gulp.task('copy-manifest', done => {
  gulp.src('./src/manifest.json')
    .pipe(gulp.dest('./build'));
  return done();
});

gulp.task('copy-icons', done => {
  gulp.src('./src/icons/**/*')
    .pipe(gulp.dest('./build/icons'));
  return done();
});

gulp.task('default', gulp.series('clean', gulp.parallel('build-js', 'copy-manifest', 'copy-icons')));