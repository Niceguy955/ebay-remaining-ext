(function showRemainingQuantity() {
  'use strict';
  const remainingQunatityField = '"remainingQty":';
  const showRemainingField = '"showQtyRemaining":false';
  const quantityElementId = 'qtySubTxt';

  let html = document.documentElement.innerHTML;
  let remainingIndex = html.indexOf(remainingQunatityField);
  if(remainingIndex !== -1) {
    let startFrom = remainingIndex + remainingQunatityField.length;
    let remaining = html.substring(startFrom, html.indexOf(',', startFrom)).trim();
    console.log('remaining: ' + remaining);
    const remainingColor = Number(remaining) < 10 ? 'red' : 'green';
    if(html.indexOf(showRemainingField) !== -1) { //show if they are trying to hide :)
      document.getElementById(quantityElementId).innerHTML = `<span style="color: blue">Remaining: <b style="color: ${remainingColor}">${remaining}</b></span>`;
    }
  }
  else {
    console.log('remaining quantity not found');
  }
}());
