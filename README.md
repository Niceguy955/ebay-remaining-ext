# Unhide eBay Quantity - a Chrome extension

## Developed by [Niceguy955](https://www.reddit.com/user/Niceguy955) 3/2019

## What is it

This Chrome extension shows the remaining quantity of an item on an eBay page, when the original lister choses to hide available quantity. It should work in any Chromimum-based browser (like Vivaldi, Brave and future Edge).

There's no magic here: eBay chose to include all relevant data *inside* the HTML code sent to the client - including data supposed to be hidden. The extension just looks for the remaining quantity, and replaces the "Limited quantity available" phrase with the remaining quantity.

## Installation

The extension is not listed in the Chrome Web Store, partially out of laziness, but mostly to not alert/piss off eBay. As such, it needs to be installed manually, and that has some side effects:

1. Download the [build.zip](https://bitbucket.org/Niceguy955/ebay-remaining-ext/downloads/build.zip) file, and expand it
1. Go to the Extensions page in your Chrom/ium browser in one of the following methods: 
    1. Hamburger menu > More Tools > Extensions
    1. Type chrome://extensions in the address bar
1. Enable "Developer Mode" (currently, top right corner, but Google likes moving this around 😊)
1. Click "Load Unpacked" (currently top left corner - see comment above)
1. Browse to the folder you expanded, and select the folder
1. You should see the extension added to the extensions page, and it's icon added to the address bar (it would look inactive)

Test the extension:

1. Browse to an eBay page with hideen quantity, and refresh the page to see the results
1. When you're on an eBay item page, the extension's icon will look active

### Side effects of installing unsigned extension

1. Every time you restart the browser, you'll get a warning asking you to "Disable developer mode extesions" - ignore it. If you disabeled the extension, you can easily re-enable it in the Extensions page.
1. The extension has to be updated manually - every time a new version comes out, you'll need to remove the old one, and re-install.

## Problems/issues/questions

Please use the Issues tab to report problems, issues, feature requests and suggestions.
You can also send me a private message at [Niceguy955](https://www.reddit.com/user/Niceguy955).

## Building the code

1. Install Node on your machine (any OS)
1. Clone this repo
1. Install NPM dependencies (`npm i` in project's folder)
1. The code will be in the `src` folder
1. Gulp is used to "build" the code - just run `npm run build` to start building
1. You can look/change the build commands in the `gulpfile.js`

## License

This extension is licensed under GPLv3.  
You can read the LICENSE file, but the TL;DR is: you can use it wherever and however you like, just include an attribution to me (Niceguy955) with a link to my profile (https://www.reddit.com/user/Niceguy955). It also stipulates you cannot blame me for any damage you incur from using this code.

**By using this product and code, you explicitly agree to the licensing term.**